# jetsontx2-python-requirements

Contains a .txt file that can install the basic python packages that comes pre-installed in Anaconda. By default anaconda does not support ARM architecture, so this file can be used to install most of the essential packages from pip.